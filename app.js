const express = require("express");
const mongoose = require("mongoose");
var bodyParser = require('body-parser');

const app = express();
// app.use(express.urlencoded({ extended: true }));

// conenction to mongodb
mongoose.connect("mongodb://127.0.0.1:27017", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
.then(()=>console.log('connected'))
.catch(e=>console.log(e));;


// middlewares

app.use(express.static("public"));
app.set("view engine", "ejs");
// configure the app to use bodyParser()
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());



// routes
app.use(require("./routes/index"))


// server configurations....
app.listen(3000, () => console.log("Server started listening on port: 3000"));
